import pandas as pd
import numpy as np

df_BLM = pd.read_pickle("/Countermovements Data & Codes/Consistency/BLM_Monthly_Top_hashtags.pkl")
df_ALM = pd.read_pickle("/Countermovements Data & Codes/Consistency/ALM_Monthly_Top_hashtags.pkl")
df_P_BLM = pd.read_pickle("/Countermovements Data & Codes/Consistency/P-BLM_Monthly_Top_hashtags.pkl")
df_WLM = pd.read_pickle("/Countermovements Data & Codes/Consistency/WLM_Monthly_Top_hashtags.pkl")


def filter_topn_hashtags(df,topn):
    # Group result_df by 'Hashtag' and sum 'N_Tweets' within each group to get total usage count
    hashtag_totals = df.groupby('Hashtag')['N_Tweets'].sum().reset_index()

    # Sort the DataFrame by total usage count in descending order
    hashtag_totals_sorted = hashtag_totals.sort_values(by='N_Tweets', ascending=False)

    # Select the top n most used hashtags
    top_n_hashtags = hashtag_totals_sorted.head(topn)
    top_n_hashtags = top_n_hashtags['Hashtag'].tolist()

    return (df[df['Hashtag'].isin(top_n_hashtags)])

def calculate_consistency_score(results_df, threshold):
    # Calculate consistency score for each hashtag in each month
    consistency_scores = []

    # Iterate over each month
    for month in results_df['Month'].unique():
        # Filter the DataFrame for the current month
        month_df = results_df[results_df['Month'] == month]

        # Iterate over each row in the DataFrame for the current month
        for index, row in month_df.iterrows():
            hashtag = row['Hashtag']
            n_tweets = row['N_Tweets']

            # Calculate consistency score based on threshold
            score = 1 if n_tweets > threshold else 0

            # Append the consistency score to the list
            consistency_scores.append({'Hashtag': hashtag, 'Month': month, 'Consistency_Score': score})

    # Convert the list of consistency scores to a DataFrame
    consistency_scores_df = pd.DataFrame(consistency_scores)

    # Calculate the total consistency score by averaging the score for all months and all hashtags
    total_consistency_score = consistency_scores_df['Consistency_Score'].mean()

    return total_consistency_score


# Determine Top N hashtag to be included in the consistency calculation up to 200
topn = 130
df_BLM = filter_topn_hashtags(df_BLM,topn)
df_ALM = filter_topn_hashtags(df_ALM,topn)
df_P_BLM = filter_topn_hashtags(df_P_BLM,topn)
df_WLM = filter_topn_hashtags(df_WLM,topn)


import matplotlib.pyplot as plt

# Define the interval of threshold values
threshold_values = range(1, 250,5)  # from 1 to 500 (inclusive)

# Calculate consistency score for each threshold value
BLM_consistency_scores = []
ALM_consistency_scores = []
P_BLM_consistency_scores = []
WLM_consistency_scores = []

for threshold in threshold_values:
    BLM_consistency_scores.append(calculate_consistency_score(df_BLM, threshold))
    ALM_consistency_scores.append(calculate_consistency_score(df_ALM, threshold))
    P_BLM_consistency_scores.append(calculate_consistency_score(df_P_BLM, threshold))
    WLM_consistency_scores.append(calculate_consistency_score(df_WLM, threshold))


# Plot the consistency scores
plt.figure(figsize=(5,5))
plt.plot(threshold_values, BLM_consistency_scores, color = "black",label = "BLM",alpha = 0.75,linewidth=4)
plt.plot(threshold_values, ALM_consistency_scores, color = "red",label = "ALM",alpha = 0.75,linewidth=4)
plt.plot(threshold_values, P_BLM_consistency_scores, color = "blue",label = "P-BLM",alpha = 0.75,linewidth=4)
plt.plot(threshold_values, WLM_consistency_scores, color = "gray",label="WLM",alpha = 0.75,linewidth=4)
plt.legend()
plt.ylim([0,1])
plt.xlabel('Threshold Value')
plt.ylabel('Consistency Score')
plt.grid(True)
plt.savefig("/Countermovements Data & Codes/Consistency/Consistency_Comparison_Plots/Consistency_Comparison_N_"+str(topn)+".pdf", format="pdf", bbox_inches="tight")