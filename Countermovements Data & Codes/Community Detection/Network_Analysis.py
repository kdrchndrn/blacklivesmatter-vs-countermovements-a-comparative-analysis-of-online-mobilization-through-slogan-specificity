import igraph as ig
from igraph import Graph
from igraph import community
import matplotlib.pyplot as plt
import random
import pandas as pd
import re
from collections import Counter

# Community detection
def detect_communities(graph):
    # Apply the Louvain method
    # Same seed is used for replicability
    random.seed(100)
    louvain_partition = graph.community_multilevel(weights=graph.es['weight'])

    # Get the communities
    communities = louvain_partition.as_cover()

    return communities

# Print the fractional sized more than 1 %
def print_fractional_sizes_more_than_0_01(communities, threshold_percentage=1,community_mapping = 0):
    # Get the total number of nodes in the graph
    total_nodes = len(communities._graph.vs)
    filtered_communities = [c for c in communities if len(c) > total_nodes * threshold_percentage / 100 ]

    # Filter and print communities with more nodes than the threshold
    for community_id, nodes in enumerate(filtered_communities):
        fractional_size = len(nodes) / total_nodes
        print(f"Community {community_mapping[communities.membership[nodes[0]][0]]}: Fractional Size = {fractional_size:.4f}")

# Calculate EI Index for each community
def calculate_ei_index(graph, communities,threshold_percentage=1,community_mapping = 0):
    filtered_communities = [c for c in communities if len(c) > graph.vcount() * threshold_percentage / 100 ]

    community_sizes = []
    community_ei = []
    community_ids =[]
    for community_id,community in enumerate(filtered_communities):        
        # Count the sum of internal and external edge weights for each node in the community
        internal_edge_weights = 0
        external_edge_weights = 0
        for vertex in community:
            for edge in graph.es[graph.incident(vertex)]:
                source, target = edge.source, edge.target
                if source in community and target in community:
                    internal_edge_weights += edge["weight"]
                elif source in community or target in community:
                    external_edge_weights += edge["weight"]

        # Calculate the E-I Index for the community
        total_edge_weights = internal_edge_weights + external_edge_weights
        if total_edge_weights > 0:
            ei_index = (external_edge_weights - internal_edge_weights) / total_edge_weights
        else:
            ei_index = 0
        print("EI calculation for Community "+str(community_id) + " is completed. (Total: "+str(len(filtered_communities))+")")
        community_ids.append(community_mapping[communities.membership[community[0]][0]])
        community_sizes.append(len(community)/graph.vcount())
        community_ei.append(ei_index)

    return pd.DataFrame(list(zip(community_ids, community_sizes,community_ei,filtered_communities)), columns=["ID",'Fractional Size', 'EI-Index',"Community Object"])

# Load the graph from the GML file
# g = Graph.Read_GML("/Countermovements Data & Codes/Community Detection/ALM_output_10000_authors_graph.gml")
# g = Graph.Read_GML("/Countermovements Data & Codes/Community Detection/P_BLM_output_10000_authors_graph.gml")
g = Graph.Read_GML("/Countermovements Data & Codes/Community Detection/BLM_output_10000_authors_graph.gml")
# g = Graph.Read_GML("/Countermovements Data & Codes/Community Detection/WLM_output_10000_authors_graph.gml")

loops = [edge.index for edge in g.es if edge.source == edge.target]
g.delete_edges(loops)
zero_weights =[edge.index for edge in g.es if edge["weight"] == 0]
g.delete_edges(zero_weights)

# Detect communities using the Louvain method
communities = detect_communities(g)

# Sort communities with increasing size, reorder labels so that labels are increasing in size
community_sizes = [len(community) for community in communities]
sorted_communities = sorted(enumerate(community_sizes), key=lambda x: x[1], reverse=True)

# Create a mapping from old community IDs to new community IDs, Communities smaller than 1% of total size labeled as N
threshold_size = 0.01 * g.vcount()
community_mapping = {old_id: new_id if size >= threshold_size else "N" for new_id, (old_id, size) in enumerate(sorted_communities, 1)}

# Add a new attribute to the graph to store the community labels
g.vs["community_label"] = [str(community_mapping[community_id[0]]) for community_id in communities.membership]

# Calculate the EI Index and plot the frequent hashtags
df = calculate_ei_index(g,communities,1,community_mapping=community_mapping)
df = df.sort_values("ID")
print(df)

import numpy as np
np.average(df["EI-Index"],weights = df["Fractional Size"])

# Select the community ids corresponding to 
# community_ids = [1,2,3,4,5,6,7,8,9,10,11,12] #ALM
# community_ids = [1,2,3,4] #P-BLM
community_ids = [1,2,3,4,5] #BLM
# community_ids = [1,2,3,4,5,6,7] #WLM



# df_tweets = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/ALM_df.pkl")
# df_tweets = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/P-BLM_df.pkl")
df_tweets = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/BLM_df.pkl")
# df_tweets = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/WLM_df.pkl")




# Plotting the top hashtags in each community

for i in range(len(community_ids)):
    community_id = community_ids[i]

    filtered_twets = df_tweets[df_tweets["AuthorID"].isin(g.vs.select(community_label = str(community_id))["label"])]
    hashtags = filtered_twets['Tweet'].apply(lambda x: re.findall(r'#(\w+)', x))
    all_hashtags = [(tag).lower() for sublist in hashtags for tag in sublist]

    removed_hashtags = ["whitelivesmatter","blacklivesmatter","bluelivesmatter","alllivesmatter","policelivesmatter","whitelivesmatters","blacklivesmatters"
        ,"bluelivesmatters","alllivesmatters","policelivesmatters","whitelifematter","blacklifematter","bluelifematter","alllifematter","policelifematter",
        "whitelivesmattter","blacklivesmattter","bluelivesmattter","alllivesmattter","policelivesmattter","alm","blm","wlm","plm"]
    filtered_hashtags = [tag for tag in all_hashtags if tag not in removed_hashtags]

        
    # Count the frequency of each hashtag
    hashtag_counts = Counter(filtered_hashtags)

    # Get the top 10 most used hashtags
    top_hashtags = dict(hashtag_counts.most_common(10))

    plt.figure(figsize=(4,4))
    plt.barh(list(top_hashtags.keys()), list(top_hashtags.values()), color='skyblue')  # Note the use of barh for horizontal bars
    file_name="ID_" + str(community_id) +"_EI_"+str(abs(round(df["EI-Index"][df["ID"]==community_id].iloc[0],2))) + ".pdf"
    plt.xlabel("Frequency")
    plt.tight_layout()
    plt.xticks(rotation=45, ha='right')
    path = "/Countermovements Data & Codes/Community Detection"
    plt.savefig(path + "/" + file_name, format="pdf", bbox_inches="tight")

