# Topic modeling (Top2Vec)

import pandas as pd
from top2vec import Top2Vec

alm=pd.read_pickle('/Countermovements Data & Codes/Data and Misc/ALM_df.pkl')
model_alm = Top2Vec(list(alm['clean_text']))
model_alm.save('/Countermovements Data & Codes/Topic Model/alm_top2vec')
model_alm.hierarchical_topic_reduction(20)
topics_words , word_scores , topic_nums = model_alm.get_topics(reduced=True)

p_blm=pd.read_pickle('/Countermovements Data & Codes/Data and Misc/P-BLM_df.pkl')
model_p_blm = Top2Vec(list(p_blm['clean_text']))
model_p_blm.save('/Countermovements Data & Codes/Topic Model/pblm_top2vec')
model_p_blm.hierarchical_topic_reduction(20)
topics_words , word_scores , topic_nums = model_p_blm.get_topics(reduced=True)

wlm=pd.read_pickle('/Countermovements Data & Codes/Data and Misc/WLM_df.pkl')
model_wlm = Top2Vec(list(wlm['clean_text']))
model_wlm.save('/Countermovements Data & Codes/Topic Model/wlm_top2vec')
model_wlm.hierarchical_topic_reduction(20)
topics_words , word_scores , topic_nums = model_wlm.get_topics(reduced=True)

blm=pd.read_pickle('/Countermovements Data & Codes/Data and Misc/BLM_sampled_filtered.pkl')
model_blm = Top2Vec(list(blm['clean_text']))
model_blm.save('/Countermovements Data & Codes/Topic Model/blm_top2vec')
model_blm.hierarchical_topic_reduction(20)
topics_words , word_scores , topic_nums = model_blm.get_topics(reduced=True)


pd.DataFrame(model_alm.get_topics(num_topics=20, reduced=True)[0]).to_excel('/Countermovements Data & Codes/Topic Model/alm_20.xlsx')
pd.DataFrame(model_p_blm.get_topics(num_topics=20, reduced=True)[0]).to_excel('/Countermovements Data & Codes/Topic Model/pblm_20.xlsx')
pd.DataFrame(model_wlm.get_topics(num_topics=20, reduced=True)[0]).to_excel('/Countermovements Data & Codes/Topic Model/wlm_20.xlsx')
pd.DataFrame(model_blm.get_topics(num_topics=20, reduced=True)[0]).to_excel('/Countermovements Data & Codes/Topic Model/blm_20.xlsx')


# Creating wordclouds

import matplotlib.pyplot as plt
from wordcloud import WordCloud

for topic_no in range(0,20):
    topic=model_alm.search_words_by_vector(model_alm.topic_vectors_reduced[topic_no],num_words=150)

    data={}

    for x in range(0,len(topic[0])):
        data[topic[0][x]]=topic[1][x]

    wc = WordCloud(background_color = "white",
                max_words = 200, max_font_size = 500,
                random_state = 42, width = 1920,
                height = 1080).generate_from_frequencies(data)

    plt.figure(figsize=(19.20, 10.80))
    plt.imshow(wc, interpolation="None")
    plt.axis('off')
    plt.savefig('/Countermovements Data & Codes/Topic Model/pblm_wordclouds/'+str(topic_no)+'_alllivesmatter.pdf', dpi=1000)
    plt.clf()

for topic_no in range(0,20):
    topic=model_p_blm.search_words_by_vector(model_p_blm.topic_vectors_reduced[topic_no],num_words=150)

    data={}

    for x in range(0,len(topic[0])):
        data[topic[0][x]]=topic[1][x]

    wc = WordCloud(background_color = "white",
                max_words = 200, max_font_size = 500,
                random_state = 42, width = 1920,
                height = 1080).generate_from_frequencies(data)

    plt.figure(figsize=(19.20, 10.80))
    plt.imshow(wc, interpolation="None")
    plt.axis('off')
    plt.savefig('/Countermovements Data & Codes/Topic Model/pblm_wordclouds/'+str(topic_no)+'_pblivesmatter.pdf', dpi=1000)
    plt.clf()

for topic_no in range(0,20):
    topic=model_wlm.search_words_by_vector(model_wlm.topic_vectors_reduced[topic_no],num_words=150)

    data={}

    for x in range(0,len(topic[0])):
        data[topic[0][x]]=topic[1][x]

    wc = WordCloud(background_color = "white",
                max_words = 200, max_font_size = 500,
                random_state = 42, width = 1920,
                height = 1080).generate_from_frequencies(data)

    plt.figure(figsize=(19.20, 10.80))
    plt.imshow(wc, interpolation="None")
    plt.axis('off')
    plt.savefig('/Countermovements Data & Codes/Topic Model/pblm_wordclouds/'+str(topic_no)+'_whitelivesmatter.pdf', dpi=1000)
    plt.clf()

for topic_no in range(0,20):
    topic=model_blm.search_words_by_vector(model_blm.topic_vectors_reduced[topic_no],num_words=150)

    data={}

    for x in range(0,len(topic[0])):
        data[topic[0][x]]=topic[1][x]

    wc = WordCloud(background_color = "white",
                max_words = 200, max_font_size = 500,
                random_state = 42, width = 1920,
                height = 1080).generate_from_frequencies(data)

    plt.figure(figsize=(19.20, 10.80))
    plt.imshow(wc, interpolation="None")
    plt.axis('off')
    plt.savefig('/Countermovements Data & Codes/Topic Model/pblm_wordclouds/'+str(topic_no)+'_blacklivesmatter.pdf', dpi=1000)
    plt.clf()
