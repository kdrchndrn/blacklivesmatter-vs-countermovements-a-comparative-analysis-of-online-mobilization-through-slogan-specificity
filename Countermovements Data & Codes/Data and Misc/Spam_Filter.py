import os
import re
import pandas as pd
import preprocessor as p

# Read data
df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/2yearsBlackLivesMatter_en.pkl")

# Filter between 2020-2022
mask = df["Time"] >= "1/1/2020" 
df = df.loc[mask]
mask = df["Time"] <= "1/1/2022"
df = df.loc[mask]

# Remove any empty rows
mask = df['Tweet'].str.len() >=1
df= df.loc[mask]
df['Tweet'] = df['Tweet'].astype('str')

# Change Timestamp to date format
df["Time"]= df['Time'].apply(lambda a: pd.to_datetime(a).date()) 

# Emoji removal is defined additionally as builtin preprocessor leaves out some of the emojis
def remove_emojis(data):
    emoj = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642" 
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
                      "]+", re.UNICODE)
    return re.sub(emoj, '', data)

# Define preprocessor to remove URL,Mentions,Emojis
def preprocess_tweet(row):
    text = row['Tweet']
    p.set_options(p.OPT.URL,p.OPT.MENTION,p.OPT.EMOJI,p.OPT.SMILEY)
    text = p.clean(text)
    text =  remove_emojis(text)
    return text

# Hashtags are only removed for spam detection
def remove_hashtags_from_clean_text (row):
    p.set_options(p.OPT.HASHTAG)
    text = row ['clean_text']
    text = p.clean (text)
    return text

# "clean_text" includes hashtags
df['clean_text'] = df.apply(preprocess_tweet, axis=1)

# "clean_text_without_hashtag" is used for spam detection & removal
df['clean_text_without_hashtag'] = df.apply(remove_hashtags_from_clean_text,axis=1)

print(df.head(10))
df.columns


# Spam Detection

spam=df[df.duplicated('clean_text_without_hashtag',keep=False)]
unique_spam=spam.drop_duplicates('clean_text_without_hashtag')

spam_index_list=[]

from collections import Counter

for index_no in unique_spam.index:
    possible_spam_dict=dict(Counter(spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]]['Time']))
    possible_dates=[]
    for key, value in possible_spam_dict.items():
        if value>=2:
            possible_dates.append(key)
    if len(possible_dates)>0:
        for possible_date in possible_dates:
            possible_spam_dict2=dict(Counter(spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]][spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]]['Time']==possible_date]['AuthorID']))
            for key, value in possible_spam_dict2.items():
                if value>=2:
                    for x in spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]][spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]]['Time']==possible_date]['AuthorID'][spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]][spam[spam['clean_text_without_hashtag']==unique_spam['clean_text_without_hashtag'][index_no]]['Time']==possible_date]['AuthorID']==key].index:
                        spam_index_list.append(x)

spam=df.loc[spam_index_list].reset_index(drop=True)
unique_spam=spam.drop_duplicates('clean_text_without_hashtag')
df=df.drop(index=spam_index_list).reset_index(drop=True)


# Save Dropped Tweets, Dropped unique tweets and the resulting df
spam.to_excel("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/BLM_dropped_tweets.xlsx")
unique_spam.to_excel("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/BLM_unique_dropped_tweets.xlsx")
df.to_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/BLM_df.pkl")
df.to_csv("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/BLM_df.csv")
