import pandas as pd
import igraph as ig
from collections import defaultdict
import scipy.sparse as sp
import re


#df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/BLM_df.pkl") 
#df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/ALM_df.pkl")
#df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/P-BLM_df.pkl")
df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/WLM_df.pkl")


df = df[["Tweet","AuthorID"]]
# Function to extract hashtags from a tweet
def extract_hashtags(tweet):
    return [tag.lower() for tag in re.findall(r'#\w+', tweet)]


# List of hashtags to exclude
exclude_hashtags = ['#blm','#blacklivesmatter','#blacklivesmatters','#blacklifematter','#blacklifematters',"#blacklivesmater",'#blacklivesmattter','#blacklivesmattters',
                    '#alm','#alllivesmatter','#alllivesmatters','#alllifematter','#alllifematters','#alllivesmater','#alllivesmattter','#alllivesmattters',
                    '#bluelm','#bluelivesmatter','#bluelivesmatters','#bluelifematter','#bluelifematters', '#bluelivesmater','#bluelivesmattter','#bluelivesmattters',
                    '#wlm','#whitelivesmatter','#whitelivesmatters','#whitelifematter','#whitelifematters','#whitelivesmater','#whitelivesmattter','#whitelivesmattters',
                    '#plm','#policelivesmatter','#policelivesmatters','#policelifematter','#policelifematters','#policelivesmater','#policelivesmattter','#policelivesmattters']
# Apply the function to create a new "Hashtag" column
df['Hashtag'] = df['Tweet'].apply(extract_hashtags)

# Exclude specific hashtags from the result DataFrame
result_df = df.explode('Hashtag')[['AuthorID', 'Hashtag']]
result_df = result_df[~result_df['Hashtag'].isin(exclude_hashtags)]
result_df = result_df[~result_df['Hashtag'].isna()]


# Take a random sample of size 10,000 from all unique AuthorID's
random_author_ids = result_df['AuthorID'].unique()
sample_author_ids = pd.Series(random_author_ids).sample(n=10000, replace=False).tolist()

# Filter the DataFrame based on the sampled AuthorID's
sample_df = result_df[result_df['AuthorID'].isin(sample_author_ids)]


df=sample_df.dropna(axis=0,how="any")
df["value"]=1

# Generate table for authors vs hashtags with binary entries if an author used the hashtag
df = df.pivot_table(index='AuthorID',columns='Hashtag',values='value',fill_value=0)

# Now we can use matrix multiplication to generate adjacency matrix for author vs. author network
X = sp.csr_matrix(df.astype(int).values) # convert dataframe to sparse matrix
Xc = X * X.T # multiply sparse matrix with its transpose
Xc.setdiag(0) # reset diagonal
g = ig.Graph.Weighted_Adjacency(Xc,mode="undirected")

# Add author IDs as labels
g.vs["label"]= list(df.index)

# Print the graph summary
g.vcount()
g.ecount()

# Save graph file as gml
ig.write(g, "/Countermovements Data & Codes/Community Detection/WLM_output_10000_authors_graph.gml", format="gml")
#ig.write(g, "/Countermovements Data & Codes/Community Detection/ALM_output_10000_authors_graph.gml", format="gml")
#ig.write(g, "/Countermovements Data & Codes/Community Detection/BLM_output_10000_authors_graph.gml", format="gml")
#ig.write(g, "/Countermovements Data & Codes/Community Detection/P-BLM_output_10000_authors_graph.gml", format="gml")

