import pandas as pd
import re
import numpy as np

Movement_Label = "BLM"

# Load the data from a pickle file
df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/"+Movement_Label+"_df.pkl")

# Remove unnecessary columns
df = df.drop(columns = ['clean_text',
       'clean_text_without_hashtag'])

df["Tweet"] = df['Tweet'].str.lower()

# Function to extract hashtags from a tweet
def extract_hashtags(tweet):
    hashtags = re.findall(r'#\w+', tweet)
    return hashtags

# Initialize an empty dictionary to store hashtag frequencies
hashtag_freq = {}

# List of hashtags to be excluded
excluded_hashtags = ["#alm","#alllivesmatter","#alllivesmatters","#alllivesmattter","#alllivesmater",
                    "#blacklivesmatter","#blacklifematters","#blacklivesmattter","#blacklivesmattters",
                    "#blm","#bluelivesmatter","#plm","#policelivesmatter",
                    "#policelivesmatters","#bluelivesmatters","#whitelivesmatters","#whitelifematters",
                    "#wlm","#whitelivesmatter","#blacklivesmatter","#blacklivesmatters","#whitelivesmatters"]

# Iterate over the data frame in chunks
list_df = np.array_split(df, 100)
for chunk in list_df:
    # Extract hashtags from each chunk
    chunk['hashtags'] = chunk['Tweet'].apply(extract_hashtags)

    # Flatten the list of hashtags in each chunk and count their frequencies
    for hashtags_list in chunk['hashtags']:
        for hashtag in hashtags_list:
            if hashtag not in excluded_hashtags:
                hashtag_freq[hashtag] = hashtag_freq.get(hashtag, 0) + 1

# Extract selected hashtags from the dictionary
for key in excluded_hashtags:
    hashtag_freq.pop(key, None)

# Sort the dictionary by values (frequencies) in descending order
sorted_hashtags = sorted(hashtag_freq.items(), key=lambda x: x[1], reverse=True)

# Get the top 200 hashtags
topn = 200
top_hashtags = [hashtag for hashtag, _ in sorted_hashtags[:topn]] 

# Convert 'Time' column to datetime
df['Time'] = pd.to_datetime(df['Time'])

# Extract month from the 'Time' column
df['Month'] = df['Time'].dt.to_period('M')
df['hashtags'] = df['Tweet'].apply(extract_hashtags)


# Initialize the dataframe for monthly number of tweets posted for each hashtag
result_df = pd.DataFrame(columns=['Hashtag', 'Month', 'N_Tweets'])
# Iterate over each month
for month in df['Month'].unique():
    print(month)
    # Filter the DataFrame for the current month
    month_df = df[df['Month'] == month]
    # Iterate over each hashtag
    for hashtag in top_hashtags:
        # Find the tweets containing the hashtag
        filtered_month_df = month_df[month_df['hashtags'].apply(lambda x: hashtag in x)]
        # Count the occurrences of the hashtag in the current month
        n_tweets = len(filtered_month_df)
        
        # Append the result to the result DataFrame
        result_df = pd.concat([pd.DataFrame([[hashtag,month,n_tweets]], columns=result_df.columns), result_df], ignore_index=True)

# Save resulting dataframe 
result_df.to_pickle("/Countermovements Data & Codes/Consistency/"+Movement_Label+"_Monthly_Top_hashtags.pkl")

