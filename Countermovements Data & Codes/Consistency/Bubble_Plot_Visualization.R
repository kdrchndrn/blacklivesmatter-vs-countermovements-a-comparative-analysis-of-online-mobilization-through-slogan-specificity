# Import Prepared Dataframes 
df_Buble_ALM<-read.csv("/Countermovements Data & Codes/Consistency/ALM_Bubble.csv")
df_Buble_P_BLM<-read.csv("/Countermovements Data & Codes/Consistency/P-BLM_Bubble.csv")
df_Buble_WLM<-read.csv("/Countermovements Data & Codes/Consistency/WLM_Bubble.csv")
df_Buble_BLM<-read.csv("/Countermovements Data & Codes/Consistency/BLM_Bubble.csv")


## Plotting
library(viridis)
library(hrbrthemes)
library(ggplot2)
library(plotly)
library(ggrepel)


bubble_plot_ALM<-ggplot(data=df_Buble_ALM,aes(x=Consistency_Score, y=percentage, size=Total,colour= Nof_Users,label=Hashtag)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1,25),limits = c(1,30100),name="Number of Tweets") +
  scale_colour_gradient(low="black", high="red", limits = c(0, 5000), oob = scales::squish,name = "Number of Unique Users")+
  #geom_text_repel(size = 3,max.overlaps = 20) + 
  theme_minimal() +
  #theme(legend.position = "none")+
  #theme(text = element_text(family = "sans"))+
  xlim(0,1)+
  ggtitle("ALM Top 50 Hashtags Consistency Score vs Active User Contribution")+
  labs(x="Consistency Score (Threshold =50)",y="Active User Contribution (ACUC)")

bubble_plot_ALM
ggsave("/Countermovements Data & Codes/Consistency/ALM_bubble_plot.pdf", plot = bubble_plot_ALM, device = "pdf", width = 9, height = 6)
bubble_plot_ALM <- ggplotly(bubble_plot_ALM)
htmlwidgets::saveWidget(bubble_plot_ALM,"/Countermovements Data & Codes/Consistency/Bubble_Plot_ALM.html")


bubble_plot_P_BLM<-ggplot(data=df_Buble_P_BLM,aes(x=Consistency_Score, y=percentage, size=Total, color=Nof_Users,label=Hashtag)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1,25),limits = c(1,30100),name="Number of Tweets") +
  scale_color_gradient(low="black", high="red", limits = c(0, 5000), oob = scales::squish,name = "Number of Unique Users")+
  #geom_text_repel(size = 3,max.overlaps =30) + 
  theme_minimal() +
  #theme(legend.position = "none")+
  #theme(text = element_text(family = "sans"))+
  xlim(0,1)+
  ggtitle("P&BLM Top 50 Hashtags Consistency Score vs Active User Contribution")+
  labs(x="Consistency Score (Threshold =50)",y="Active User Contribution (ACUC)")

bubble_plot_P_BLM
ggsave("/Countermovements Data & Codes/Consistency/P_BLM_bubble_plot.pdf", plot = bubble_plot_P_BLM, device = "pdf", width = 9, height = 6)
bubble_plot_P_BLM <- ggplotly(bubble_plot_P_BLM)
htmlwidgets::saveWidget(bubble_plot_P_BLM,"/Countermovements Data & Codes/Consistency/Bubble_Plot_P_BLM.html")


bubble_plot_WLM<-ggplot(data=df_Buble_WLM,aes(x=Consistency_Score, y=percentage, size=Total,color=Nof_Users,label=Hashtag)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1,25),limits = c(1,31000),name="Number of Tweets") +
  scale_color_gradient(low="black", high="red", limits = c(0, 5000), oob = scales::squish,name = "Number of Unique Users")+
  #geom_text_repel(size = 3,max.overlaps =30) + 
  theme_minimal() +
  #theme(legend.position = "none")+
  #theme(text = element_text(family = "sans"))+
  xlim(0,1)+
  ggtitle("WLM Top 50 Hashtags Consistency Score vs Active User Contribution")+
  labs(x="Consistency Score (Threshold =50)",y="Active User Contribution (ACUC)")

bubble_plot_WLM
ggsave("/Countermovements Data & Codes/Consistency/WLM_bubble_plot.pdf", plot = bubble_plot_WLM, device = "pdf", width = 9, height = 6)
bubble_plot_WLM <- ggplotly(bubble_plot_WLM)
htmlwidgets::saveWidget(bubble_plot_WLM,"/Countermovements Data & Codes/Consistency/Bubble_Plot_WLM.html")


bubble_plot_BLM<-ggplot(data=df_Buble_BLM,aes(x=Consistency_Score, y=percentage, size=Total,color=Nof_Users,label=Hashtag)) +
  geom_point(alpha=0.7) +
  scale_size(range = c(1,25),limits = c(1,250000),name="Number of Tweets") +
  scale_color_gradient(low="black", high="red", limits = c(0, 40000), oob = scales::squish,name = "Number of Unique Users")+
  #geom_text_repel(size = 3,max.overlaps =30) + 
  theme_minimal() +
  theme(text = element_text(family = "sans"))+
  #theme(legend.position = "none")+
  xlim(0,1)+
  ggtitle("BLM Top 50 Hashtags Consistency Score vs Active User Contribution")+
  labs(x="Consistency Score (Threshold =50)",y="Active User Contribution (ACUC)")


bubble_plot_BLM
ggsave("/Countermovements Data & Codes/Consistency/BLM_bubble_plot.pdf", plot = bubble_plot_BLM, device = "pdf", width = 9, height = 6)
bubble_plot_BLM <- ggplotly(bubble_plot_BLM)
htmlwidgets::saveWidget(bubble_plot_BLM,"/Countermovements Data & Codes/Consistency/Bubble_Plot_BLM.html")
