from igraph import Graph
import igraph as ig
from sklearn.metrics import silhouette_score
import numpy as np


# Load graph from the GML file
graph = Graph.Read_GML("Countermovements Data & Codes/Community Detection/BLM_output_10000_authors_graph.gml")
graph = Graph.Read_GML("Countermovements Data & Codes/Community Detection/ALM_output_10000_authors_graph.gml")
graph = Graph.Read_GML("Countermovements Data & Codes/Community Detection/P_BLM_output_10000_authors_graph.gml")
graph = Graph.Read_GML("Countermovements Data & Codes/Community Detection/WLM_output_10000_authors_graph.gml")



loops = [edge.index for edge in graph.es if edge.source == edge.target]
graph.delete_edges(loops)
zero_weights =[edge.index for edge in graph.es if edge["weight"] == 0]
graph.delete_edges(zero_weights)


# Leiden Algorithm
leiden_communities = graph.community_leiden(weights=graph.es["weight"])

# Louvain algorithm
louvain_communities = graph.community_multilevel(weights='weight')

# Infomap algorithm
infomap_communities = graph.community_infomap(edge_weights='weight')

# Walktrap algorithm
walktrap_communities = graph.community_walktrap(weights='weight').as_clustering()

# Label Propagation algorithm
label_propagation_communities = graph.community_label_propagation(weights='weight')


# Modularity
leiden_modularity = leiden_communities.modularity
louvain_modularity = louvain_communities.modularity
infomap_modularity = infomap_communities.modularity
walktrap_modularity = walktrap_communities.modularity
label_propagation_modularity = label_propagation_communities.modularity

# Print Modularity
print("Leiden Modularity:", leiden_modularity)
print("Louvain Modularity:", louvain_modularity)
print("Infomap Modularity:", infomap_modularity)
print("Walktrap Modularity:", walktrap_modularity)
print("Label Propagation Modularity:", label_propagation_modularity)
