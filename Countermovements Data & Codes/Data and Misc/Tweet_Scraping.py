# Tweet scraping

from twarc import Twarc2, expansions
import datetime
import json

dataset_list=['#WhiteLivesMatter','#BlueLivesMatter','#PoliceLivesMatter','#AllLivesMatter','#BlackLivesMatter']

first_date=datetime.datetime(2020, 1, 1, 0, 0, 0, 0, datetime.timezone.utc)
last_date=datetime.datetime(2022, 1, 1, 0, 0, 0, 0, datetime.timezone.utc)

for data_name in dataset_list:
    query = data_name + " lang:en -is:retweet"
    client = Twarc2(bearer_token="XXXXX")
    tweets_list=[]
    search_results = client.search_all(query=query, start_time=first_date, end_time=last_date, max_results=100)
    for page in search_results:
        result = expansions.flatten(page)
        for tweet in result:
            tweets_list.append(tweet)
    with open(data_name + '.json', 'w') as fp:
        json.dump(tweets_list, fp)

